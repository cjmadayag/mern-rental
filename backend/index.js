const app = require("express")();
const {urlencoded,json,static} = require("express");
const cors = require("cors");
const {port, dbURL} = require("./src/config");
const {connect} = require("mongoose");

connect(dbURL,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useFindAndModify:false
},()=>console.log("Database Connection Successful"));

app.use(cors());
app.use(urlencoded({extended:false}));
app.use(json());

app.listen(port,()=>console.log(`Listening to Port ${port}`));

const {join} = require("path");
app.use(static(join(__dirname,"public")));

const Auth = require("./src/middlewares/Auth");
const UserRoute = require("./src/routes/UserRoute");
const ItemRoute = require("./src/routes/ItemRoute");
const BookingRoute = require("./src/routes/BookingRoute");
app.use("/",[Auth,UserRoute,ItemRoute,BookingRoute]);






// const multer = require("multer");

// const storage = multer.diskStorage({
//     destination:(req,file,callback)=>{
//       callback(null, "public/images/uploads")
//     },
//     filename:(req,file,callback)=>{
//       callback(null, Date.now() + "-" + file.originalname);
//     }
// })

// const upload = multer({storage});

// app.post("/upload",upload.single("image"),(req,res)=>{
//   if(req.file){
//     res.json({imageUrl:`images/uploads/${req.file.filename}`});
//   }else{
//     res.status(401).send("Bad Request");
//   }
// })