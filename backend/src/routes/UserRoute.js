const Router = require("express").Router();
const UserModel = require("../models/User");
const {genSaltSync,hashSync,compareSync} = require("bcryptjs");
const {sign} = require("jsonwebtoken");
const {secret} = require("../config")

Router.post("/adduser",async(req,res)=>{

  const password = hashSync(req.body.password,genSaltSync(10));

  let user = UserModel({
    username:req.body.username,
    firstName:req.body.firstName,
    lastName:req.body.lastName,
    email:req.body.email,
    password
  });

  user=await user.save();
  res.send(user);
});

Router.post("/login",async(req,res)=>{
  const user = await UserModel.findOne({username:req.body.user});
  
  if(!user) return res.send({message:"No user found"})

  if(!compareSync(req.body.password,user.password)){
    return res.send({message:"Incorrect password"});
  }
  
  const loggedInUser = {
    firstName:user.firstName,
    lastName:user.lastName,
  }
  const token = sign({id:user._id},secret);

  res.send({token,loggedInUser});
})

module.exports=Router;
