const Router = require("express").Router();
const BookingModel = require("../models/Booking");
const stripe = require("stripe")("sk_test_hiZiuW77inhHzbmkZh5mAJgH006SjWwKdU");

Router.patch("/processpayment",async(req,res)=>{
  const paymentIntent = await stripe.paymentIntents.create({
    amount: 1099,
    currency: 'php',
    // customer: 
    metadata: {integration_check: 'accept_a_payment'},
  });

  console.log(paymentIntent)

  res.send(paymentIntent)
})


Router.post("/book", async(req,res)=>{
  let book = new BookingModel({
    item: req.body.itemId,
    rentee: req.body.renteeId,
    startDate: req.body.startDate,
    endDate: req.body.endDate,
    amount: req.body.amount
  })

  book = await book.save();
  res.send(book);

})

Router.get("/paidbookings", async(req,res)=>{
  const books = await BookingModel.find({status:"Paid"});
  res.send(books);
})

Router.post("/mybookings", async(req,res)=>{
  const books = await BookingModel.find({rentee:req.body.renteeId}).populate(["item"])
  res.send(books);
})

Router.delete("/cancelbooking", async(req,res)=>{
  const book = await BookingModel.findByIdAndDelete(req.body.id);
  res.send(book);
})

Router.patch("/paybooking", async(req,res)=>{
  const book = await (await BookingModel.findByIdAndUpdate(req.body.id,{status:"Processing Payment"},{new:true}))
  .populate("rentee");
  
  
  res.send(book)
  const paymentIntent = await stripe.paymentIntents.create({
    amount: book.amount,
    currency: 'php',
    metadata: {integration_check: 'accept_a_payment'},
  });

 

  console.log(paymentIntent)

  res.send(paymentIntent)



})



module.exports = Router;