const Router = require("express").Router();
const ItemModel = require("../models/Item");

Router.post("/additem",async(req,res)=>{
  let item = ItemModel({
    name:req.body.name,
    description:req.body.description,
    price:req.body.price,
    imagePath:req.body.imagePath,
    owner:req.body.ownerId
  });
  item = await item.save();
  res.send(item);
})

const multer = require("multer");

const storage = multer.diskStorage({
  destination:(req,file,callback)=>{
    callback(null, "public/images/uploads")
  },
  filename:(req,file,callback)=>{
    callback(null, Date.now() + "_" + file.originalname);
  }
})
 
const upload = multer({storage});

Router.post("/uploadimage",upload.single("image"),(req,res)=>{
  if(req.file){
    res.json(`images/uploads/${req.file.filename}`)
  }else{
    res.status(401).json("Bad Request");
  }
})

Router.get("/items",async(req,res)=>{
  const items = await ItemModel.find().populate("owner");
  res.send(items)
})

const fs = require("fs")

Router.delete("/deleteitem",async(req,res)=>{
  const item = await ItemModel.findByIdAndDelete(req.body.id);

  await fs.unlinkSync("public/"+item.imagePath);
  res.send(item)
})

module.exports=Router;