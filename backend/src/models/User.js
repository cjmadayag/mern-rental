const {Schema,model} = require("mongoose");

const UserSchema = new Schema({
  username:String,
  firstName:String,
  lastName:String,
  email:String,
  password:String,
  isAdmin:{
    type:Boolean,
    default:false
  }
},{timestamps:true});

module.exports=model("User",UserSchema);