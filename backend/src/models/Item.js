const {Schema,model} = require("mongoose");

const ItemSchema = new Schema({
  name:String,
  description:String,
  price:Number,
  imagePath:String,
  owner:{
    type:Schema.Types.ObjectId,
    ref:"User"
  }
})

module.exports=model("Item",ItemSchema);