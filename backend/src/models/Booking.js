const {Schema,model} = require("mongoose");

const BookingSchema = new Schema({
  item: {
    type: Schema.Types.ObjectId,
    ref: "Item"
  },
  rentee: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  startDate: String,
  endDate: String,
  status: {
    type:String,
    default:"Pending"
  },
  amount: Number
})

module.exports = model("Booking",BookingSchema);