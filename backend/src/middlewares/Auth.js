const Router = require("express").Router();
const {verify} = require("jsonwebtoken");
const {secret} = require("../config");

Router.post("/auth",(req,res,next)=>{
  if(!req.body.token) return res.json("invalid user");
  try{
    res.send({user:verify(req.body.token,secret)});
  }catch(e){
    return res.json("invalid user");
  }
})

module.exports=Router;