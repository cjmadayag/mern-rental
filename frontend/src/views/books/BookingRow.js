import React from "react";
import {ListGroupItem,Col, CardBody, CardTitle, Row, CardText, Button} from "reactstrap";
import moment from "moment"
import numeral from "numeral"


const BookingRow = ({booking,onBookingCancel,onBookingPayment})=>{
  const {_id,item,startDate,endDate,status,amount} = booking;

  const handleButtonCancel = ()=>{
    onBookingCancel(_id);
  }

  const handleButtonPayment = ()=>{
    onBookingPayment(_id);
  }

  return(
    <ListGroupItem>
      <Row>
        <Col
          md={3}
          style={{height:"20vh"}}
        >
          <img 
            src={"http://localhost:4000/"+item.imagePath}
            style={{
              width:"100%",
              height:"100%"
            }}
            alt=""
          />
        </Col>
        <Col
          md={5}
        >
          <CardBody>
            <CardTitle
              className="h2"
            >{item.name}</CardTitle>
            <CardText>Description: {item.description}</CardText>
            <CardText>Booked date/s: {moment(startDate).format('LL')}{startDate!==endDate && " - "+ moment(endDate).format("LL")}</CardText>
    
            
          </CardBody>

        </Col>
        <Col
          md={4}
          className="border-left"
        >
          <CardBody>
            <CardText>Status: {status}</CardText>
            <CardText>Amount: Php {numeral(amount).format("0,0.00")}</CardText>
            <Button
              className="mr-1"
              onClick={handleButtonPayment}
            >Pay</Button>
            <Button
              onClick={handleButtonCancel}
            >Cancel</Button>
          </CardBody>
        </Col>
      </Row>
    </ListGroupItem>
  )
}

export default BookingRow;