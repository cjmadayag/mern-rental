import React, { useEffect,useState } from "react";
import Sidebar from "../../components/Sidebar"
import {Col,Card,CardHeader,CardBody, ListGroup} from "reactstrap";
import BookingRow from "./BookingRow";
import Stripe from "../../components/Stripe";

import {Elements, useStripe} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';

const stripePromise = loadStripe('pk_test_kbituzPfw4RyQrkfKGTCyfvc00OwuBWhhT');

const Bookings = ()=>{

  const [bookings,setBookings] = useState([]);
  const [isStripeOpen,setIsStripeOpen] = useState(false)
  const [bookingToPay, setBookingToPay] = useState("");

  useEffect(()=>{
    const req = {
      method:"POST",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({renteeId:sessionStorage.userId})
    }

    fetch("http://localhost:4000/mybookings",req)
    .then(res=>res.json())
    .then(res=>setBookings(res))
  },[])

  const handleBookingCancel = (id)=>{
    const req = {
      method: "DELETE",
      headers: {"content-type":"application/json"},
      body: JSON.stringify({id})
    }

    fetch("http://localhost:4000/cancelbooking",req)
    .then(res=>res.json())
    .then(res=>{
      const bookingsCopy = [...bookings];

      const newBookings = bookingsCopy.filter(book=>(
        book._id !== res._id
      ))

      setBookings(newBookings);
    })
  }

  const handleStripeToggle = (id)=>{
    !!id ? setBookingToPay(id) : setBookingToPay("")

    setIsStripeOpen(!isStripeOpen)
  }

  // const 

  const handleBookPayment = async(stripe,elements,CardElement)=>{
    const req = {
      method:"PATCH",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({id:bookingToPay})
    }

    let res = await fetch("http://localhost:4000/processpayment",req)
    res = await res.json();
    // const {client_secret} =req
    // const {status} = await stripe.confirmCardPayment(client_secret, {
    //   payment_method: {
    //     card: elements.getElement(CardElement),
    //     billing_details: {
    //       name: sessionStorage.firstName + " " + sessionStorage.lastName,
    //     },
    //   }
    // });

    // if(status="succeeded"){
    //   const req = {
    //       method:"PATCH",
    //       headers:{"content-type":"application/json"},
    //       body:JSON.stringify({id})
    //     }

    //   fetch("http://localhost:4000/paybooking",req)
    //   .then(res=>res.json())
    //   .then(res=>{
    //     const bookingsCopy = bookings;
    //     const newBookings = bookingsCopy.map(book=>{
    //       if(book._id===id) book.status = "Paid";
    //       return book;
    //     })

    //     setBookings(newBookings);
    //   })
    // }else{
    //   Error("Payment was not successful")
    // }

  
  }

  
  return(
    <Sidebar
      setChildren={
        <Col
          lg={{size:8,offset:1}}
        >
          <Card>
            <CardHeader className="h1 text-center bgDarkGreen">
            Booked Items
            </CardHeader>
            <CardBody>
              <ListGroup>
                {bookings.map(booking=>(
                  <BookingRow
                    key={booking._id}
                    booking={booking}
                    onBookingPayment={handleStripeToggle}
                    onBookingCancel={handleBookingCancel}
                  />
                ))}
              </ListGroup>
            </CardBody>
          </Card>
          <Elements
            stripe={stripePromise}
          >
            <Stripe
              isOpen={isStripeOpen}
              toggle={handleStripeToggle}
              onFormSubmit={handleBookPayment}
            />
          </Elements>
        </Col>
        
      }
    />
  )
}

export default Bookings;