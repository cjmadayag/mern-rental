import React,{useState, useEffect} from 'react';
import {Col,Card,Form,FormGroup,Label,Input,Button} from "reactstrap";
import {Error} from "../../components/Toaster";

const Login = ()=>{
  const [user,setUser] = useState("");
  const [password,setPassword] = useState("");

  useEffect(()=>{
    sessionStorage.clear()
  },[])

  const handleLogin = ()=>{
    if(!sessionStorage) return window.location.replace("/login");

    const req = {
      method:"POST",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({user,password})
    }

    fetch("http://localhost:4000/login",req)
    .then(res=>res.json())
    .then(res=>{
      if(res.message===undefined){
        sessionStorage.token = res.token
        sessionStorage.firstName = res.loggedInUser.firstName;
        sessionStorage.lastName = res.loggedInUser.lastName;
        sessionStorage.userId = res.loggedInUser.id;
        window.location.replace("/home")
      }else{
        Error(res.message);
      }
    })
  }

  return (
    <div
      className="d-flex justify-content-center align-items-center vh-100 bgBlack"
    >
      <Col
       lg={5}
      >
        <Card
          className="text-center p-5"
        >
          <h1
            className="pb-2"
          >Login</h1>
          <Form>
            <FormGroup>
              <Label>Username/Email</Label>
              <Input
                type="text"
                onChange={e=>setUser(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label>Password</Label>
              <Input
                type="text"
                onChange={e=>setPassword(e.target.value)}
              />
            </FormGroup>
            <Button
              className="buttonGreen"
              onClick={handleLogin}
            >
              Login
            </Button>
          </Form>
        </Card>
      </Col>
      
    </div>
  );
}

export default Login;
