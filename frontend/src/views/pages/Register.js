import React,{useState} from "react";
import { Col,Card,CardHeader,CardBody, FormGroup, Label,Input,Button } from "reactstrap";
import {Error} from "../../components/Toaster";

const Register = ()=>{

  const [firstName,setFirstName] = useState("");
  const [lastName,setLastName] = useState("");
  const [username,setUsername] = useState("");
  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [confirmPassword,setConfirmPassword] = useState("");
  const [showWarning, setShowWarning] = useState(false);

  const handleRegisterUser = ()=>{
    if(firstName===""||lastName===""||username===""||email===""
      ||password===""||password!==confirmPassword) 
      return Error("Please complete the form");

    const req = {
      method:"POST",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({firstName,
        lastName,
        username,
        email,
        password
      })
    }

    fetch("http://localhost:4000/adduser",req)
    .then(res=>res.json())
    .then(res=>{
      window.location.replace("/login");
    })
  }

  return(
    <div
      className="d-flex justify-content-center align-items-center vh-100 bgBlack"
    >
      <Col
        lg={4}
      >
        <Card>
          <CardHeader
            className="h2 text-center bgDarkGreen"
          >Register</CardHeader> 
          <CardBody
            className="p-5"
          >
            <FormGroup>
              <Label>First Name</Label>
              <Input
                onChange={e=>setFirstName(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label>Last Name</Label>
              <Input
                onChange={e=>setLastName(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label>Username</Label>
              <Input
                onChange={e=>setUsername(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input
                type="email"
                onChange={e=>setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label>Password</Label>
              <Input
                type="password"
                onChange={e=>{
                  setPassword(e.target.value)
                  password!==e.target.value ? setShowWarning(true) : setShowWarning(false)  
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>Confirm Password</Label>
              <Input
                type="password"
                onChange={e=>{
                  setConfirmPassword(e.target.value)
                  password!==e.target.value ? setShowWarning(true) : setShowWarning(false)
                }}
              />
              { showWarning ? <Label>Password does not match</Label>:"" }
            </FormGroup>
            <Button 
              className="buttonGreen"
              onClick={handleRegisterUser}
            >Register
            </Button>
          </CardBody>
        </Card>
      </Col>
    </div>
  )
}

export default Register;