import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import {Col,Card,CardBody,Button, CardHeader, ListGroup,} from "reactstrap";
import {Error} from "../../components/Toaster"
import ItemForm from "./ItemForm";
import ItemRow from "./ItemRow"
import moment from "moment";


const Item = ()=>{
  const [showForm,setShowForm] = useState(false);
  const [items,setItems] = useState([])

  const [books,setBooks] = useState([])

  useEffect(()=>{
    fetch("http://localhost:4000/items")
    .then(res=>res.json())
    .then(res=>setItems(res))

    fetch("http://localhost:4000/paidbookings")
    .then(res=>res.json())
    .then(res=>setBooks(res));
  },[])

  const toggleShowForm = ()=>{
    setShowForm(!showForm);
  }

  const handleSaveItem = async({name,description,price,image})=>{

    const data = new FormData();
    data.append("image",image)

    let res = await fetch("http://localhost:4000/uploadimage",{
      method: "POST",
      body: data
    })
    res= await res.json()
    
    const req = {
      method:"POST",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({
        name,
        description,
        price,
        imagePath:res,
        ownerId:sessionStorage.userId
      })
    }

    fetch("http://localhost:4000/additem",req)
    .then(res=>res.json())
    .then(res=>{
      setItems([...items,res])
    })

    toggleShowForm();
  }

  const handleDeleteItem = (id)=>{
    const itemsCopy = [...items];
    const req = {
      method:"DELETE",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({id})
    }

    fetch("http://localhost:4000/deleteitem",req)
      .then(res=>res.json())
      .then(res=>{
        const newItems = itemsCopy.filter(item=>(
          item._id !== res._id
        ))
        setItems(newItems)
      })
  }

  const handleRent = ({from,to},{_id,price})=>{

    if(!from){
      Error("Please select date/s")
    }else{
      !to && (to=from)

      let dateDiff = moment(to).diff(from,"day")

      !dateDiff ? dateDiff=1:dateDiff+=1;
      
      price *= dateDiff;
      
      const req = {
        method:"POST",
        headers:{"content-type":"application/json"},
        body:JSON.stringify({
          itemId: _id,
          renteeId: sessionStorage.userId,
          startDate: from,
          endDate: to,
          amount: price
        })
      }

      fetch("http://localhost:4000/book",req)
      .then(res=>res.json())
      .then(res=>{
        window.location.replace("/mybookings")
      })
    }
  }


  return(
    <Sidebar
      setChildren={
        <Col
          lg={{size:8,offset:1}}
        >
          <Card>
            <CardHeader className="h1 text-center bgDarkGreen">
            My Rentals
            </CardHeader>
            <CardBody>
              <Button
                color="secondary"
                onClick={toggleShowForm}
              >Add</Button>
              <ItemForm
                showForm={showForm}
                toggleShowForm={toggleShowForm}
                handleSaveItem={handleSaveItem}
              />
              <hr/>
              <ListGroup>
                {items.map(item=>(
                  <ItemRow
                    key={item._id}
                    item={item}
                    books={books}
                    handleDeleteItem={handleDeleteItem}
                    handleRent={handleRent}
                  />
                ))}
              </ListGroup>
            </CardBody>
          </Card>
            
        </Col>
      }
    />
  )
}

export default Item;