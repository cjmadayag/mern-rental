import React, { useState } from "react";
import {Modal,ModalHeader,ModalBody, FormGroup,Label, Input,Button} from "reactstrap"
import {Error} from "../../components/Toaster";

const ItemForm = ({showForm,toggleShowForm,handleSaveItem})=>{
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [price,setPrice] = useState(0);
  const [image,setImage] = useState(null)

  const handleImage = (e)=>{
    const img = e.target.files[0]
      
    if(img.name.match(new RegExp(".(jpg|jpeg|png|gif)$","ig"))){
      setImage(img);
    }else{
      Error("Select correct image format");
      e.target.value = null;
    }
  }

  const saveButton = ()=>{
    if(name==="" || description==="" || price<=0 || image===null){
      return Error("Complete the form")
    }
    handleSaveItem({name,description,price,image})
  }

  return(
    <Modal
      isOpen={showForm}
    >
      <ModalHeader
        toggle={toggleShowForm}
        className="text-center"
      >Add Item
      </ModalHeader>
      <ModalBody
        className="px-4"
      >
        <FormGroup>
          <Label>Name</Label>
          <Input
            onChange={e=>setName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label>Description</Label>
          <Input
            onChange={e=>setDescription(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label>Price per day</Label>
          <Input
            type="number"
            onChange={e=>setPrice(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label>Image</Label>
          <Input
            type="file"
            onChange={handleImage}
          />
        </FormGroup>
        <Button
          className="my-2"
          onClick={saveButton}
        >Save</Button>
        <Button
          className="ml-2"
          onClick={toggleShowForm}
        >Cancel</Button>
      </ModalBody>
    </Modal>
  )
}

export default ItemForm;