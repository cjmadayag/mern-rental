import React, { useEffect, useState } from "react";
import {ListGroupItem,Col, CardBody, CardTitle, Row, CardText, Button} from "reactstrap";
import DayPicker from "../../components/DayPicker"
import moment from "moment";

const ItemRow = ({item,handleDeleteItem,handleRent,books})=>{

  const [disabledDays,setDisabledDays] = useState([])
  const [disabledDaysArray,setDisabledDaysArray] = useState([])


  useEffect(()=>{
    const booksCopy = books;
    const disabledDays = booksCopy.map(book=>{
      if(book.item === item._id){
        return {from:new Date(book.startDate),to:new Date(book.endDate)}
      }
    })
    setDisabledDays(disabledDays);

    const newDisabledDaysArray = []
    disabledDays.forEach(({from,to})=>{
      if(+from===!to){
        newDisabledDaysArray.push(+new Date(from));
      }else{
        let fromCopy = new Date(from)
        while(+fromCopy <= +new Date(to)){
          newDisabledDaysArray.push(+fromCopy)
          fromCopy=new Date(fromCopy.setDate(fromCopy.getDate()+1))
        }
      }
    })

    setDisabledDaysArray(newDisabledDaysArray);
  },[])

  const deleteButton = ()=>{
    handleDeleteItem(item._id)
  }

  return(
    <ListGroupItem>
      <Row>
        <Col
          md={3}
          style={{height:"20vh"}}
        >
          <img 
            src={"http://localhost:4000/"+item.imagePath}
            style={{
              width:"100%",
              height:"100%"
            }}
            alt=""
          />
        </Col>
        <Col
          md={5}
        >
          <CardBody>
            <CardTitle
              className="h2"
            >{item.name}</CardTitle>
            <CardText>Description: {item.description}</CardText>
            <CardText>Price: {item.price}</CardText>
            <CardText>Owner: {item.owner.username}</CardText>
          </CardBody>

        </Col>
        <Col
          md={4}
        >
          <CardBody>
            <DayPicker
              dropdownLabel="Check/select dates"
              handleRent={handleRent}
              buttonLabel="Rent"
              item={item}
              disabledDays={[{before:new Date},...disabledDays]}
              disabledDaysArray={disabledDaysArray}
            />

            {/* if admin */}
            <Button
              onClick={deleteButton}
            >
              Delete
            </Button>
          </CardBody>
        </Col>
      </Row>
    </ListGroupItem>
  )
}

export default ItemRow;