import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// bootstrap
import "bootstrap/dist/css/bootstrap.css"

// toastify
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

// fontawesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

toast.configure({
  autoClose:2500,
  draggable:false,
  hideProgressBar:true,
  pauseOnHover:false,
  pauseOnFocusLoss:false
})

library.add(fas)

ReactDOM.render(
  // <React.StrictMode>
    <App />,
  // </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
