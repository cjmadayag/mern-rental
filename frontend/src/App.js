import React,{lazy,Suspense} from 'react';
import {BrowserRouter,Switch,Route} from "react-router-dom";
import Loader from "./components/Loader";

const App = ()=>{

  const Login = lazy(()=>import("./views/pages/Login"));
  const Home = lazy(()=>import("./views/pages/Home"));
  const Items = lazy(()=>import("./views/items/Items"));
  const Bookings = lazy(()=>import("./views/books/Bookings"));
  const Register = lazy(()=>import("./views/pages/Register"));

  return(
    

    <BrowserRouter>
      <Suspense
        fallback={<Loader/>}
      >
        <Switch>
          <Route
            path="/login"
            component={Login}
          />
          <Route
            path="/register"
            component={Register}
          />
          <Route
            path="/home"
            component={Home}
          />
          <Route
            path="/items"
            component={Items}
          />
          <Route
            path="/mybookings"
            component={Bookings}
          />
        </Switch>
      </Suspense>
    </BrowserRouter>

  )
}

export default App;
