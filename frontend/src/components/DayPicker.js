import React, { useState } from 'react';
import ReactDayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import {Dropdown,DropdownToggle,DropdownMenu,Button} from "reactstrap";
import {Error} from "./Toaster";

const DayPicker = ({dropdownLabel,buttonLabel,handleRent,item,disabledDays,disabledDaysArray}) => {

  const initialState = { from: null, to: null, enteredTo: null }
  const [dateRange, setDateRange] = useState(initialState);
  const [showDropdown,setShowDropdown] = useState(false)

  const {from,to,enteredTo} = dateRange;

  const isSelectingFirstDay = (from, to, day) => {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }

  const handleDayClick = (day,{disabled}) => {

    if(disabled) return

    if (from && to && day >= from && day <= to) {
      handleResetClick();
      return;
    }

    if (isSelectingFirstDay(from, to, day)) {
      setDateRange({
        from: day,
        to: null,
        enteredTo: null,
      });
    } else {

      const dateString = from.toString();
      const dateFrom = new Date(dateString)

      let checkDate = from;

      while( +checkDate < +day ){
        checkDate = new Date(checkDate.setDate(checkDate.getDate()+1))
        
        if(disabledDaysArray.includes(+checkDate)){
          console.log("true")
          Error("Dates are not available")
          handleResetClick()
          return
        }
      }
 
      setDateRange({
        from:dateFrom,
        to: day,
        enteredTo: day,
      });
    }
  }

  // const handleDayMouseEnter = (day) => {
  //   const { from, to } = dateRange;
  //   if (!isSelectingFirstDay(from, to, day)) {
  //     setDateRange({
  //       from,
  //       to,
  //       enteredTo: day,
  //     });
  //   }
  // }

  const handleResetClick = () => {
    setDateRange(initialState);
  }

  console.log(disabledDays)
  return (
    <Dropdown isOpen={showDropdown} toggle={()=>setShowDropdown(!showDropdown)}>
      <DropdownToggle caret>{dropdownLabel}</DropdownToggle>
      <DropdownMenu>
        <ReactDayPicker
          // className="Range"
          numberOfMonths={2}
          fromMonth={from}
          selectedDays={[from, { from, to: enteredTo }]}
          disabledDays={disabledDays}
          // modifiers={{ start: from, end: enteredTo }}
          onDayClick={handleDayClick}
          // onDayMouseEnter={handleDayMouseEnter}
        />
        <Button
          block
          onClick={()=>{
            setShowDropdown(false)
            handleRent(dateRange,item)
          }}
        >{buttonLabel}</Button>
      </DropdownMenu>
    </Dropdown>
  );
}

export default DayPicker;