import {toast} from "react-toastify";

export const Error = (message)=>(
  toast.error(message)
)