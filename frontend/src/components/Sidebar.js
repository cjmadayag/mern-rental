import React, { useEffect } from "react";
import ReactSidebar from "react-sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Link} from "react-router-dom";
import {ListGroup,ListGroupItem} from "reactstrap";

const navList = [
    {
      name:"Home",
      link:"/home",
      icon:"home"
    },
    {
      name:"Items",
      link:"/items",
      icon:"box"
    },
    {
      name:"Bookings",
      link:"/mybookings",
      icon:"bookmark"
    },
    {
      name:"Logout",
      link:"/login",
      icon:"sign-out-alt"
    }
]

const Sidebar = ({setChildren})=>{

  useEffect(()=>{
    validateUser();
  },[])

  const validateUser = ()=>{
    if(!sessionStorage.token) window.location.replace("/login");

    const req = {
      method:"POST",
      headers:{"content-type":"application/json"},
      body:JSON.stringify({token:sessionStorage.token})
    }
    fetch("http://localhost:4000/auth",req)
    .then(res=>res.json())
    .then(res=>{
      if(!res.user) return window.location.replace("/login")
      sessionStorage.userId = res.user.id
    })
  }

  return(
  <ReactSidebar
    sidebarClassName="bgBlack text-white"
    children={setChildren}
    docked={true}
    transitions={false}
    sidebar={
      <div
        style={{width:"22vw"}}
        className="d-flex flex-column align-items-center vh-100 pt-5"
      >
        <h2 className="textGreen pb-5 pt-5 mt-5 text-center">Welcome to myRental</h2>
        <ListGroup
         style={{width:"50%"}}
        >
          {navList.map((item,index)=>(
            <Link
              key={index}
              to={item.link}
              className="textGreen py-2"
              style={{textDecoration:"none"}}
            >  
              <ListGroupItem
                className="navLink d-flex justify-content-center align-items-center"
                
              >
                <div
                  className="h-100 text-center d-flex justify-content-center align-items-center" 
                  style={{width:"30%"}}
                >
                  <FontAwesomeIcon
                    icon={item.icon}
                    size="2x"
                  />
                </div>
                <div
                  className="h-100"
                  style={{width:"70%"}}
                >
                  <h4 className="my-auto">{item.name}</h4>
                </div>
                
                
              </ListGroupItem>
            </Link>
          ))}
        </ListGroup>
      </div>
    
    }
  />
  )
}

export default Sidebar;