import React from "react";
import {CardElement,useStripe,useElements} from '@stripe/react-stripe-js';
import {Modal,ModalHeader,ModalBody,Form,Button} from "reactstrap";

const Stripe = ({isOpen,toggle,onFormSubmit})=>{
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = (e)=>{
    e.preventDefault();
    const cardElement = elements.getElement(CardElement);
    onFormSubmit(stripe,elements,cardElement)
    
  }

  return(
    <Modal
      isOpen={isOpen}
    >
      <ModalHeader
        toggle={toggle}
      >Payment</ModalHeader>
      <ModalBody>
        <Form 
          onSubmit={handleSubmit}
        >
          <CardElement/>
          <Button
            type="submit"
          >submit</Button>
        </Form>
      </ModalBody>
    </Modal>
  )
}

export default Stripe;